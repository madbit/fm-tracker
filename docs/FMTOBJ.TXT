
ID            'FMT_XXXX'
SongLength    Byte
Patterns      Byte
Instruments   Byte
MasterVolume  Byte
Tempo         Byte
Filler        5 Bytes

PosTable      =SongLength

<Si SongLength es impar agregar un byte aca>

InstData      Instruments * 010h
PattOffset    OFFSET desde el ID desde hasta el comienzo de cada pattern

Pattern STRUC
  Tracks      Byte
  Rows        Byte
  Data        4 * (Tracks * Rows)
End

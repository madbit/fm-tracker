# Madbit's FM Tracker

This is a [tracker](https://en.wikipedia.org/wiki/Music_tracker) for MS-DOS that uses the [OPL-2](https://en.wikipedia.org/wiki/Yamaha_OPL#OPL2) chips present in [AdLib](https://en.wikipedia.org/wiki/Ad_Lib,_Inc.) compatible cards to generate sound using FM synthesis.

It was mainly developed using [Borland Pascal](https://en.wikipedia.org/wiki/Turbo_Pascal) (with some assembler bits by Keywiz) in a 386dx with a [Sound Blaster Pro](https://en.wikipedia.org/wiki/Sound_Blaster#Second-generation_Sound_Blasters.2C_16-bit_ISA_.26_MCA_cards) card, and even though it wasn't 100% finished, it was functional enough to create 3 demo songs which are included in the package.

I originally made this back in 1994 when I was 17 and active in the argentine demoscene, but I will not be developing this any further, so I'm posting it here in case anybody is interested in continuing it. Just let me know.

NOTE: This has been tested on current hardware and it's confirmed working using DOSBox 0.74 under Windows 7 x64.

## Demo

You can see a brief demo video here: http://youtu.be/xQQvtjI7THg

## Features

* Supports OPL-2 compatible sound cards like AdLib, SoundBlaster, SB Pro, SB16 or an emulator like DOSBox.
* FastTracker inspired interface and keyboard shortcuts.
* OPL-2 Instrument editor with live preview.
* Imports instruments from HSC Tracker and S!P AdLib Tracker.
* Up to 256 instruments per song.
* Up to 256 patterns per song.
* 64 rows and 9 channels per pattern.
* Editable scrolltext message per song.
* Supported tracker commands: Pitch slide up, Pitch slide down, Shift to note, Set carrier volume, Set modulator volume, Set feedback, Slide volume, Jump to position, Set volume, Break pattern, Key off, Vibrato on/off, Amplitude modulation on/off, Fine volume slide up, Fine volume slide down, Set tempo.